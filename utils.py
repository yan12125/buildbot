import json
import pathlib

SCRIPT_ROOT = pathlib.Path(__file__).parent.resolve()

# load actual data
with open(SCRIPT_ROOT / 'config.json', 'r') as f:
    config = json.load(f)
