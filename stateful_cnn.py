import json

from twisted.internet import defer

from buildbot.changes.filter import ChangeFilter
from buildbot.config import BuilderConfig
from buildbot.process.factory import BuildFactory
from buildbot.process.properties import Interpolate, Property, Transform
from buildbot.process.results import SUCCESS, WARNINGS
from buildbot.schedulers.basic import SingleBranchScheduler
from buildbot.schedulers.forcesched import ForceScheduler
from buildbot.schedulers.triggerable import Triggerable
from buildbot.steps.shell import ShellCommand
from buildbot.steps.source.github import GitHub
from buildbot.steps.trigger import Trigger
from buildbot.steps.worker import CompositeStepMixin

def get_python():
    return Interpolate('%(prop:python_venv_executable)s')

# Create multiple builders, otherwise all virtual builders share the same physical builder and are run sequentially
# See: https://github.com/buildbot/buildbot/issues/7125
NUM_BUILDERS = 50

# Dynamic Trigger [1] and Virtual Builders [2] allows using configurations from sources.
# Here I roughly follow the structure of TravisTrigger in buildbot_travis [3].
# Note that documentation says getFileContentFromWorker is in CommandMixin, while it's actually in CompositeStepMixin [4].
# [1] https://docs.buildbot.net/latest/manual/configuration/steps/trigger.html#dynamic-trigger
# [2] https://docs.buildbot.net/latest/manual/configuration/builders.html#virtual-builders
# [3] https://github.com/buildbot/buildbot_travis/blob/d3bf36dc6a5741c0ca02522cc7606c5504268fae/buildbot_travis/steps/spawner.py#L25
# [4] https://github.com/buildbot/buildbot/issues/4085
class StatefulCNNTrigger(Trigger, CompositeStepMixin):
    def __init__(self, schedulerNames, **kwargs):
        self.config = None

        Trigger.__init__(self, schedulerNames=schedulerNames, **kwargs)

    @defer.inlineCallbacks
    def run(self):
        config_data = yield self.getFileContentFromWorker('utils/buildbot-config.json', abandonOnFailure=True)

        self.config = json.loads(config_data)

        rv = yield Trigger.run(self)

        return rv

    def getSchedulersAndProperties(self):
        ret = []
        for config_idx, config in enumerate(self.config):
            properties = {
                'virtual_builder_name': config['builder_name'],
                'stateful_cnn_command_env': json.dumps(config['command_env']),
            }

            ret.append({
                'sched_name': self.schedulerNames[config_idx % len(self.schedulerNames)],
                'props_to_set': properties,
                'unimportant': False,
            })

        return ret

def config_builder(c, worker_names: list[str]) -> None:
    f = BuildFactory()

    f.addStep(ShellCommand(
        name='Test',
        # workername is a property provided by buildbot
        # https://docs.buildbot.net/current/manual/configuration/properties.html
        command=[get_python(), Interpolate('../../%(prop:workername)s-preparation/build/utils/run-test.py')],
        env=Transform(lambda s: json.loads(s), Property('stateful_cnn_command_env')),
        # Any exit code not present in the dictionary will be treated as FAILURE
        # https://docs.buildbot.net/3.6.0/manual/configuration/steps/shell_command.html
        decodeRC={
            0: SUCCESS,
            # Non-termination, see exp/run-intermittently.py
            3: WARNINGS,
        },
        warnOnWarnings=True,
    ))

    for builder_index in range(NUM_BUILDERS):
        c['builders'].append(BuilderConfig(
            name=f'stateful-cnn-builder-{builder_index}',
            workernames=worker_names,
            factory=f,
            tags=['stateful_cnn'],
            collapseRequests=False,
        ))

def config_preparation_builder(c, worker_name: str) -> str:
    f = BuildFactory()
    f.addStep(GitHub(
        repourl='ssh://git@github.com/EMCLab-Sinica/NodPA.git',
        submodules=True,
        timeout=60, retry=(20, 3),
    ))
    f.addStep(ShellCommand(
        name='Upgrade pip',
        command=[get_python(), '-m', 'pip', 'install', '--upgrade', 'pip', 'setuptools'],
    ))
    f.addStep(ShellCommand(
        name='Install dependencies',
        command=[get_python(), '-m', 'pip', 'install', '-r', 'requirements.txt'],
    ))

    builder_name = f'{worker_name}-preparation'
    c['builders'].append(BuilderConfig(
        name=builder_name,
        workernames=[worker_name],
        factory=f,
        tags=['stateful_cnn'],
    ))

    return builder_name

def config_builders(c, worker_names: list[str]) -> None:
    config_builder(c, worker_names)

    preparation_builder_names = []
    build_scheduler_names = []
    for worker_name in worker_names:
        preparation_builder_names.append(config_preparation_builder(c, worker_name))

    for builder_index in range(NUM_BUILDERS):
        scheduler_name = f'stateful-cnn-scheduler-{builder_index}'
        c['schedulers'].append(Triggerable(
            name=scheduler_name,
            builderNames=[f'stateful-cnn-builder-{builder_index}'],
        ))
        build_scheduler_names.append(scheduler_name)

    f = BuildFactory()
    # Need to clone the repo on the dispatcher worker as the latter needs the config file from the repo
    f.addStep(GitHub(
        repourl='ssh://git@github.com/EMCLab-Sinica/NodPA.git',
        mode='full',
        shallow=True,
        method='clobber',
        submodules=False,
        timeout=60, retry=(20, 3),
    ))
    f.addStep(Trigger(schedulerNames=['stateful-cnn-preparation'], waitForFinish=True))
    f.addStep(StatefulCNNTrigger(schedulerNames=build_scheduler_names))

    c['builders'].append(BuilderConfig(
        name='stateful-cnn-dispatcher',
        workernames=['local_worker'],
        factory=f,
        tags=['stateful_cnn'],
    ))

    c['schedulers'].extend([
        Triggerable(
            name='stateful-cnn-preparation',
            builderNames=preparation_builder_names,
        ),
        ForceScheduler(
            name='force-stateful-cnn',
            builderNames=['stateful-cnn-dispatcher'],
        ),
        SingleBranchScheduler(
            name='stateful-cnn-github',
            change_filter=ChangeFilter(
                branch='master', repository='https://github.com/EMCLab-Sinica/NodPA'),
            builderNames=['stateful-cnn-dispatcher'],
        ),
    ])
